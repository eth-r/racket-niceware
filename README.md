# niceware

[![Build Status](https://gitlab.com/promethea_o/racket-niceware/badges/master/build.svg)](https://gitlab.com/promethea_o/racket-niceware/commits/master)
[![coverage report](https://codecov.io/gl/promethea_o/racket-niceware/branch/master/graph/badge.svg)](https://codecov.io/gl/promethea_o/racket-niceware)

A Typed Racket port of the [Niceware](https://github.com/diracdeltas/niceware) library for generating random-yet-memorable passwords. Each word provides 16 bits of entropy, so a useful password requires at least 3 words.

Because the wordlist is of exactly size 2^16, Niceware is also userful for convert cryptographic keys and other sequences of random bytes into human-readable phrases. With Niceware, a 128-bit key is equivalent to an 8-word phrase.

Demo (JS version): https://diracdeltas.github.io/niceware/

## Sample use cases

* Niceware can be used to generate secure, semi-memorable, easy-to-type
  passphrases. A random 3-5 word phrase in Niceware is equivalent to a strong
  password for authentication to most online services. For instance,
  `+8svofk0Y1o=` and `bacca cavort west volley` are equally strong (64 bits of
  randomness).
* Niceware can be used to display cryptographic key material in a way that
  users can easily backup or copy between devices. For instance, the 128-bit
  random seed used to generate a 256-bit ECC key (~equivalent to
  a 3072-bit RSA key) is only 8 Niceware words. With this 8-word phrase, you
  can reconstruct the entire public/private keypair.

## Usage in Racket

To install:

```shell
raco pkg install niceware
```

To generate an 8-byte passphrase:

```racket
#lang typed/racket

(require niceware)

; The number of bytes must be even
(define passphrase (generate-passphrase 8))

; Result: '("deathtrap" "stegosaur" "nilled" "nonscheduled")
```

## Usage in browser

The Racketscript integration of Niceware is pending, in the meanwhile use the [JS version](https://github.com/diracdeltas/niceware).

## Other

Niceware is also available as a third-party Chrome extension, thanks to Noah
Feder. https://chrome.google.com/webstore/detail/niceware-password/dhnichgmciickpnnnhfcljljnfomadag

## Docs

* niceware
    * (bytes->passphrase bytes) ⇒ <code>(Listof String)</code>
    * (passphrase->bytes words) ⇒ <code>Bytes</code>
    * (generate-passphrase size) ⇒ <code>(Listof String)</code>

### niceware
**Module**

#### (bytes->passphrase bytes) -> <code>(Listof String)</code>
Converts a byte array into a passphrase.

| Param | Type | Description |
| --- | --- | --- |
| bytes | <code>Bytes</code> | The bytes to convert |

#### (passphrase->bytes words) -> <code>Bytes</code>
Converts a phrase back into the original byte array.

| Param | Type | Description |
| --- | --- | --- |
| words | <code>(Listof String)</code> | The words to convert |

#### (generate-passphrase size) -> <code>(Listof String)</code>
Generates a random passphrase with the specified number of bytes.
NOTE: `size` must be an even number; the current maximum is 1024 bytes.

| Param | Type | Description |
| --- | --- | --- |
| size | <code>Integer</code> | The number of random bytes to use |


## Credits

Niceware was originally written in Javascript by [Yan Zhu (@diracdeltas)](https://github.com/diracdeltas).
