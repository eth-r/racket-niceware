#lang scribble/manual
@(require (for-label racket
                     niceware)
          scribble/eval)

@(define niceware-eval
    (make-eval-factory '(niceware)))
@title{Niceware}
@author{promethea Ow0}

@defmodule[niceware]

A Typed Racket port of the @hyperlink["https://github.com/diracdeltas/niceware"]{Niceware}
library for generating random-yet-memorable passwords. Each word provides 16 bits of entropy,
so a useful password requires at least 3 words.

Because the wordlist is of exactly size 2^16, Niceware is also userful for converting
cryptographic keys and other sequences of random bytes into human-readable phrases.
With Niceware, a 128-bit key is equivalent to an 8-word phrase.

@hyperlink["https://diracdeltas.github.io/niceware/"]{Demo (JS version)}

@section{Sample Use Cases}

@itemlist[
  @item{
    Niceware can be used to generate secure, semi-memorable, easy-to-type
    passphrases. A random 3-5 word phrase in Niceware is equivalent to a strong
    password for authentication to most online services. For instance,
    @racket{+8svofk0Y1o=} and @racket{bacca cavort west volley} are equally strong
    (64 bits of randomness).
  }
  @item{
    Niceware can be used to display cryptographic key material in a way that
    users can easily backup or copy between devices. For instance, the 128-bit
    random seed used to generate a 256-bit ECC key (~equivalent to
    a 3072-bit RSA key) is only 8 Niceware words. With this 8-word phrase, you
    can reconstruct the entire public/private keypair.
  }
]

@section{API Documentation}

@defproc[(bytes->passphrase [bts bytes?])
         (listof string?)]{
  Converts @racket[bts] into a passphrase.
  The @racket[bytes-length] of @racket[bts] must be @racket[even?].
}

@defproc[(passphrase->bytes [lst (listof string?)])
         bytes?]{
  Converts a phrase back into the original bytes.
}

@defproc[(generate-passphrase [x (and/c integer? even?)])
         (listof string?)]{
  Generates a random passphrase with the specified number of bytes,
  up to the current maximum of 1024 bytes.
}

@(examples
  #:eval (niceware-eval)
  (generate-passphrase 8)
  (bytes->passphrase (bytes 0 0 17 212 12 140 90 247 46 83 254 60 54 169 255 255))
  (passphrase->bytes
  '("A" "bioengineering" "Balloted" "gobbledegooK" "cReneled" "Written" "depriving" "zyzzyva"))
  (bytes->passphrase (make-bytes 3))
  (passphrase->bytes '("You" "love" "ninetales"))
  (generate-passphrase 1026))

@section{Usage in Browser}

The Racketscript integration of Niceware is under development, in the meanwhile use the
@hyperlink["https://github.com/diracdeltas/niceware"]{Javascript version}.

@section{Other}

Niceware is also available as a @hyperlink["https://chrome.google.com/webstore/detail/niceware-password/dhnichgmciickpnnnhfcljljnfomadag"]{third-party Chrome extension}, thanks to Noah
Feder.

@section{Credits}

Niceware was originally written in Javascript by @hyperlink["https://github.com/diracdeltas"]{Yan Zhu}.
