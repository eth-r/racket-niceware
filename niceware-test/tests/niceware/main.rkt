#lang typed/racket/base

(require typed/rackunit
         typed/rackunit/text-ui
         typed/rackunit/docs-complete
         (only-in typed/racket
                  make-list
                  thunk
                  string-join string-split)
         niceware)

(define-syntax-rule (throws-an exn-type body)
  (check-exn exn-type (thunk body)))

(: stringify : (Listof String) -> String)
(define (stringify strings)
  (string-join strings " "))

(run-tests
 (test-suite
  "generate-passphrase"

  (test-case "generates passphrases of correct length"
    (check-equal? 1 (length (generate-passphrase 2)))
    (check-equal? 0 (length (generate-passphrase 0)))
    (check-equal? 10 (length (generate-passphrase 20)))
    (check-equal? 256 (length (generate-passphrase 512))))

  (test-case "throws when generating passphrase with an odd number of bytes"
    (throws-an #rx"even"
               (generate-passphrase 1))
    (throws-an #rx"even"
               (generate-passphrase 23)))

  (test-case "throws when generating passphrase with >1024 bytes"
    (throws-an #rx"1024"
               (generate-passphrase 1026)))))

(run-tests
 (test-suite
  "bytes->passphrase"

  (test-case "when input has odd length"
    (throws-an #rx"even"
               (bytes->passphrase (make-bytes 1))))

  (test-case "returns expected passphrases"
    (check-equal? 0 (length (bytes->passphrase (make-bytes 0))))
    (check-equal? (list "a")
                  (bytes->passphrase (bytes #x00 #x00)))
    (check-equal? (list "zyzzyva")
                  (bytes->passphrase (bytes #xff #xff)))
    (check-equal? "a bioengineering balloted gobbledegook creneled written depriving zyzzyva"
                  (stringify (bytes->passphrase
                              (bytes 0 0 17 212 12 140 90 247 46 83 254 60 54 169 255 255)))))))

(run-tests
 (test-suite
  "passphrase->bytes"

  (test-case "when input is not in the wordlist"
    (throws-an #rx"ninetales"
               (passphrase->bytes (string-split "You love ninetales"))))

  (test-case "returns expected bytes"
    (check-equal? (bytes #x00 #x00)
                  (passphrase->bytes '("a")))
    (check-equal? (bytes #xff #xff)
                  (passphrase->bytes '("zyzzyva")))
    (check-equal? (bytes 0 0 17 212 12 140 90 247 46 83 254 60 54 169 255 255)
                  (passphrase->bytes
                   (string-split "A bioengineering Balloted gobbledegooK cReneled Written depriving zyzzyva"))))))
